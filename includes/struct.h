/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 01:29:15 by luccasim          #+#    #+#             */
/*   Updated: 2014/06/25 17:43:13 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct		s_conf_file
{
	char			*name;
	int				write;
}					t_conf_file;

t_conf_file			*ft_conf_file_new(char *name, int write);
t_conf_file			*ft_conf_file_sglt(char *name, int write, int flag);
int					ft_conf_file_del(t_conf_file **file);

#endif