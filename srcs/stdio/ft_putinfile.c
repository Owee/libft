/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putinfile.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 19:31:44 by luccasim          #+#    #+#             */
/*   Updated: 2014/06/25 22:23:34 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "struct.h"

int		ft_putinfile(char *str)
{
	t_conf_file		*conf;

	conf = ft_conf_file_sglt(NULL, 0, 0);
	if (!conf || !str)
		return (ERROR);
	ft_infile(str, conf->name, conf->write);
	return (SUCCESS);
}