/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/01 19:42:30 by luccasim          #+#    #+#             */
/*   Updated: 2014/03/16 18:44:12 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int			ft_putnbr(int nb)
{
	char	*new;
	int		ret;

	new = ft_itoa(nb);
	ret = ft_putstr(new);
	ft_strdel(&new);
	return (ret);
}