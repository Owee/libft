/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/01 19:47:03 by luccasim          #+#    #+#             */
/*   Updated: 2014/03/16 18:44:33 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int		ft_putnbr_fd(int nb, int fd)
{
	char	*number;
	int		ret;

	number = ft_itoa(nb);
	ret = ft_putstr_fd(number, fd);
	ft_strdel(&number);
	return (ret);
}
