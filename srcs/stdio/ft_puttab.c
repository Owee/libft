/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puttab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 00:03:18 by luccasim          #+#    #+#             */
/*   Updated: 2014/05/10 13:01:42 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>

int		ft_puttab(char **tab)
{
	int		ret;

	if (!tab)
		return (ft_putendl(NULL));
	if (!*tab)
		return (ft_putendl(NULL));
	ret = 0;
	while (*tab)
	{
		ret += ft_putendl(*tab);
		tab++;
	}
	return (ret);
}
