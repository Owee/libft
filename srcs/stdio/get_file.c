/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_file.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/13 22:32:50 by luccasim          #+#    #+#             */
/*   Updated: 2014/06/18 22:57:21 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		test_file(char *file)
{
	if (ft_file_exist(file))
	{
		if (access(file, R_OK) == 0)
			return (SUCCESS);
		return (FAIL);
	}
	return (FAIL);
}

static char		*ft_realloc(char *old, char *add)
{
	char		*new;

	if (!old)
		new = ft_strdup(add);
	else
	{
		new = ft_strjoin(old, add);
		ft_strdel(&old);
	}
	return (new);
}

int				get_file(char *file, char **str)
{
	char		line[BUF_SIZE + 1];
	char		*new;
	int			ret;
	int			fd;

	if (test_file(file) == FAIL)
		return (FAIL);
	ft_bzero(line, BUF_SIZE + 1);
	new = NULL;
	if ((fd = open(file, O_RDONLY)) == -1)
		return (ft_error("open() fail in get_file()", NULL));
	while ((ret = read(fd, line, BUF_SIZE)))
	{
		if (ret == ERROR)
			return (ft_error("read() fail in get_file()", NULL));
		line[ret] = 0;
		new = ft_realloc(new, line);
		*str = new;
		ft_bzero(line, BUF_SIZE + 1);
	}
	return (SUCCESS);
}