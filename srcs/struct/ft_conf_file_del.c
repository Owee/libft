/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conf_file_del.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 01:29:15 by luccasim          #+#    #+#             */
/*   Updated: 2014/06/25 17:43:13 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "struct.h"
#include "libft.h"

int		ft_conf_file_del(t_conf_file **del)
{
	if (!del)
		return (-1);
	ft_strdel(&((*del)->name));
	free(*del);
	*del = NULL;
	return (0);
}