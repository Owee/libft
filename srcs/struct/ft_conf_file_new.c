/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conf_file_new.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 01:29:15 by luccasim          #+#    #+#             */
/*   Updated: 2014/06/25 17:43:13 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "struct.h"
#include "libft.h"

t_conf_file		*ft_conf_file_new(char *file, int write)
{
	t_conf_file		*new;

	new = (t_conf_file *)malloc(sizeof(t_conf_file));
	if (new)
	{
		new->name = ft_strdup(file);
		new->write = write;
	}
	else
		ft_error("malloc() fail in ft_conf_file_new()", NULL);
	return (new);
}