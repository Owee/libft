/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conf_file_sglt.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 01:29:15 by luccasim          #+#    #+#             */
/*   Updated: 2014/06/25 17:43:13 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "struct.h"
#include <stdlib.h>

t_conf_file		*ft_conf_file_sglt(char *name, int write, int flag)
{
	static t_conf_file		*new = NULL;

	if (flag == 0)
	{
		if (new == NULL)
			new = ft_conf_file_new("tmp", 0);
	}
	else if (flag)
	{
		if (new)
			ft_conf_file_del(&new);
		new = ft_conf_file_new(name, write);
	}
	return (new);
}