/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_infile.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: luccasim <luccasim@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 19:31:44 by luccasim          #+#    #+#             */
/*   Updated: 2014/06/25 22:23:34 by luccasim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		test_file(char *str)
{
	if (ft_file_exist(str))
	{
		if (access(str, W_OK) == 0)
			return (1);
		else
			return (ft_error(str, ": Permission denied"));
	}
	return (1);
}

static int		open_file(char *file, int ret, int overwrite)
{
	if (ret == 1 && !overwrite)
		return (open(file, O_CREAT | O_WRONLY | O_APPEND, 0644));
	else if (ret == 1 && overwrite)
		return (open(file, O_CREAT | O_WRONLY | O_TRUNC, 0644));
	return (-1);
}

int				ft_infile(char *str, char *file, int overwrite)
{
	int			fd;
	int			ret;

	if (!str || !file)
		return (-1);
	if (!(ret = test_file(file)))
		return (-1);
	if ((fd = open_file(file, ret, overwrite)) == -1)
		return (ft_error("Open fail()", NULL));
	ft_putendl_fd(str, fd);
	if (close(fd) == -1)
		return (ft_error("Close fail()", NULL));
	return (0);
}